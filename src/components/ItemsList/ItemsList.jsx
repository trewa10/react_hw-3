import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Card from '../Card/Card';

const ItemWrapper = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    gap: 30px;
    padding-bottom: 45px; 
`

const ItemsList =({itemsList, openModal, addToFavorite, textBtn, widthBtn}) => {
    const cardsRender = itemsList.map(({name, price, imgUrl, article, color, isInFavorite}, index) => {
        return (
            (<Card data={article} key={(article + '' + index)} isInFavorite={isInFavorite} name={name} price={price} imgUrl={imgUrl} 
            article={article} color={color} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn} widthBtn={widthBtn}/>)
        )
    })
    return (
        <>
            <ItemWrapper>
                {cardsRender}
            </ItemWrapper>
        </>
    )
}

ItemsList.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    widthBtn: PropTypes.string
}

export default ItemsList