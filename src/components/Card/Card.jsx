import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Button from '../Button';
import {ReactComponent as FavoriteIcon} from './images/favorite.svg'

const CardElement = styled.div`
    box-sizing: border-box;
    width: 300px;
    height: 370px;
    border: 1px solid grey;
    border-radius: 10px;
    padding: 10px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`
const CardSection = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
`
const CardMain = styled(CardSection)`
    flex-direction: column;
    align-items: flex-start;
` 
const CardFooter = styled(CardSection)`
    /* position: absolute;
    bottom: 5px; */
` 
const StyledSpan = styled.span`
    font-size: ${props => props.fontSize};
` 
const ImgContainer = styled.div`
    margin: 5px 15px;
    align-self: flex-end;
    min-height: 200px;
    display: flex;
    align-items: center;
`

const ItemIMG = styled.img`
    width: 150px;
    height: auto;
`

const Card = ({name, price, imgUrl, article, color, openModal, data, isInFavorite, addToFavorite, textBtn, widthBtn}) => {
    return (
        <CardElement data-article={data} className='card'>
            <CardSection>
                <span>{name}</span>
                <Button width='auto' height='auto' onClick={addToFavorite}>
                    <div>
                        <FavoriteIcon fill={isInFavorite ? 'orange' : 'white'} stroke={isInFavorite ? 'orange' : 'grey'}/>
                    </div>
                </Button>
            </CardSection>
            <CardMain>
                <StyledSpan fontSize='12px'>Article: {article}</StyledSpan>
                <ImgContainer>
                    <ItemIMG src={imgUrl} alt={name} />
                </ImgContainer>
                <StyledSpan>Color: {color}</StyledSpan>
            </CardMain>
            <CardFooter>
                <div>{price}$</div>
                <Button backgroundColor='grey' width={widthBtn} onClick={openModal}>{textBtn}</Button>
            </CardFooter>
        </CardElement>
    )
}



Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    imgUrl: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    isInFavorite: PropTypes.bool,
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string
}

export default Card