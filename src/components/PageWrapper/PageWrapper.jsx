import React from "react";
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Container from "../Container";

const Wrapper = styled.div`
width: 100%;
height: 100%;
margin: 64px auto;
padding: 20px 0;
`

const PageWrapper = ({children}) => {
    return (
    <Wrapper>
        <Container>
            {children}
        </Container>
    </Wrapper>  
    )
}

PageWrapper.propTypes = {
    children: PropTypes.any
}

export default PageWrapper