import PropTypes from 'prop-types';
import styled from 'styled-components';
import PageWrapper from "../../components/PageWrapper/PageWrapper";
import ItemsList from "../../components/ItemsList";
import BackBtn from '../../components/BackBtn';

const Price = styled.p`
    width: max-content;
    margin: 0px auto 20px;
    font-size: 1.3rem;
`


const CartPage = (props) => {
    const {itemsList, openModal, addToFavorite, textBtn, widthBtn, goBack} = props;
    const totalPrice = itemsList.reduce((acc, item) => acc + +item.price, 0)
    return (
        <PageWrapper>
            <h2><BackBtn goBack={goBack} />Your cart</h2>
            <Price>You have {itemsList.length} item(s) with total value {totalPrice}$</Price>
            <ItemsList itemsList={itemsList} openModal={openModal} addToFavorite={addToFavorite} textBtn={textBtn} widthBtn={widthBtn}/>
        </PageWrapper>
    )
}

CartPage.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func,
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string,
    goBack: PropTypes.func
}

export default CartPage;