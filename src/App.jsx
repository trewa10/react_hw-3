import React, {useEffect, useState} from 'react';
import {createGlobalStyle} from 'styled-components';
import {Route, Routes, useNavigate} from 'react-router-dom';
import Button from './components/Button';
import Modal from './components/Modal';
import Header from './components/Header';
import Footer from './components/Footer';
import MainPage from './pages/MainPage/MainPage';
import CartPage from './pages/Cart/CartPage';
import FavoritePage from './pages/Favorite/FavoritePage';

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  #root {
    display: flex;
    flex-direction: column;
    flex: 1 1 auto;
    height: 100%;
  }
`

const App = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [goodsCollection, setGoodsCollection] = useState([]);
  const [cart, setCart] = useState([]);
  const [tryToCart, setTryToCart] = useState({});
  const [tryToRemoveCart, setTryToRemoveCart] = useState({});
  const [favorite, setFavorite] = useState([]);
  const [modalText, setModalText] = useState('');
  const [modalTitle, setModalTitle] = useState('');
  const [needRemoveItem, setNeddRemoveItem] = useState(false);
  

  useEffect(() => {
    sendRequest();
    getCart();
    getFavorite();
  }, []);

  useEffect(() => {
    if (isModalOpen) modalShow();
  }, [isModalOpen]);

  const sendRequest = () => {
    fetch(`${document.location.origin}/externalGoods.JSON`)
    .then((response) => response.json())
    .then((data) => {
      // setGoodsCollection(data);
      makeFavorite(data);
    })
  }

  const makeFavorite = (data) => {
    // let allGoodsCollection = [...goodsCollection];
    // let favCollection = [...favorite];

    let allGoodsCollection = data;
    let favCollection =  JSON.parse(localStorage.getItem('favorite'));
    if(favCollection) {
      allGoodsCollection.forEach(item => {
        favCollection.forEach(fav => {
          if(item.article === fav.article) {
            item.isInFavorite = true;
          }
        })
      })
    }
    setGoodsCollection(allGoodsCollection);
  }

  const openModal = () => {
    setIsModalOpen(true);
  }

  const modalShow = () => document.querySelector('dialog').showModal();

  const closeModal = () => {
    setIsModalOpen(false);
  }
  
  const confirmAddItem = () => {
    let newCart = [...cart];
    let item = {...tryToCart};
    newCart.push(item);
    setCart(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const confirmRemoveItem = () => {
    let newCart = [...cart];
    let item = {...tryToRemoveCart};
    let index = newCart.findIndex((card) => card.article === item.article);
    newCart.splice(index, 1)
    setCart(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
    closeModal();
  }

  const addToCart = (event) => {
    let article = +event.target.closest('.card').dataset.article;
    let item = goodsCollection.filter(item => item.article === article)[0];
    setTryToCart(item);
    setModalText(item.name);
    setModalTitle('Do you want to add to cart?');
    setNeddRemoveItem(false);
    openModal();
  }

  const removeFromCart = (event) => {
    let article = +event.target.closest('.card').dataset.article;
    let item = cart.filter(item => item.article === article)[0];
    setTryToRemoveCart(item);
    setModalText(item.name);
    setModalTitle('Do you want to remove from cart?');
    setNeddRemoveItem(true);
    openModal();
  }

  const addToFavorite = (event) => {
    let article = +event.target.closest('.card').dataset.article;
    let newGoodsCollection = [...goodsCollection];
    let newCart = [...cart];
    
    newGoodsCollection.forEach(item => {
      if(item.article === article) {
        item.isInFavorite = !item.isInFavorite;
      }
    });

    newCart.forEach(item => {
      if(item.article === article) {
        item.isInFavorite = !item.isInFavorite;
      }
    });

    let newFavorite = newGoodsCollection.filter(item => item.isInFavorite);
    setFavorite(newFavorite);
    localStorage.setItem('favorite', JSON.stringify(newFavorite));

    setGoodsCollection(newGoodsCollection);

    setCart(newCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
  }
  
  const getCart = () => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if(cart && cart.length > 0) {
      setCart(cart);
    }
  }

  const getFavorite = () => {
    let favorite = JSON.parse(localStorage.getItem('favorite'));
    if(favorite) {
      setFavorite(favorite);
    }
  }

  const navigate = useNavigate();
  const goBack = () => navigate(-1);

  return (
    <>
      <GlobalStyle/>
      <Header cartAmount={cart.length} favoriteAmount={favorite.length}/>
      <Routes>
        <Route index element={<MainPage itemsList={goodsCollection} openModal={addToCart} addToFavorite={addToFavorite} textBtn='Add to cart' />} />
        <Route path='/cart' element={<CartPage itemsList={cart} openModal={removeFromCart} addToFavorite={addToFavorite} textBtn='Remove from cart' widthBtn='135px' goBack={goBack}/>} />
        <Route path='/favorite' element={<FavoritePage itemsList={favorite} openModal={addToCart} addToFavorite={addToFavorite} textBtn='Add to cart' goBack={goBack}/>} />
      </Routes>
      
      {isModalOpen && (<Modal closeButton={true} actions={
        <>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={needRemoveItem ? confirmRemoveItem : confirmAddItem}>Yes</Button>
          <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={closeModal}>Cancel</Button>
        </>
      } closeClick={closeModal} backgroundColor='#aeb5b8' header={modalTitle} text={modalText}>
        
      </Modal>)}
      
      <Footer/>
    </>
  )
}



export default App;
